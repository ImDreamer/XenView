﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using CsvHelper;
using Microsoft.AspNetCore.Components;
using Microsoft.EntityFrameworkCore;
using Microsoft.JSInterop;
using Radzen;
using Radzen.Blazor;
using XenView.Data;

namespace XenView.Pages {
	public partial class Index {
		public const double NotificationTime = 2000;
		private List<ExtraInfo> _extraInfo = Enum.GetValues<ExtraInfo>().ToList();
		private List<string> _bookTypes = Enum.GetValues<BookType>().Select(type => AddBook.FormatName(type.ToString())).ToList();
		
		private readonly List<Category> _categories = Enum.GetValues<Category>().Skip(1).ToList();
		private readonly Dictionary<Category, bool> _activeCategories = new();
		private AddBook _addBook;

		private List<Book> _books = new();
		private RadzenDataGrid<Book> _grid;

		private bool _showDelivered = true;
		private bool _showNone = true;
		private bool _showOrdered = true;
		private bool _showRead = true;
		private int _selectedRating;
		private LogicalFilterOperator _selectedFilterMode = LogicalFilterOperator.Or;

		[Inject] private DialogService DialogService { get; set; }

		[Inject] private NotificationService NotificationService { get; set; }

		[Inject] private DatabaseContext DatabaseContext { get; set; }

		[Inject] private IJSRuntime JSRuntime { get; set; }

		public static string GenerateColor(Status data, Book book) {
			return GenerateColor(book.Status == data);
		}


		private void ToggleNone() {
			_showNone = !_showNone;
			UpdateFilter();
		}

		private void ToggleRead() {
			_showRead = !_showRead;
			UpdateFilter();
		}

		private void ToggleOrdered() {
			_showOrdered = !_showOrdered;
			UpdateFilter();
		}

		private void ToggleDelivered() {
			_showDelivered = !_showDelivered;
			UpdateFilter();
		}

		private void ToggleCategory(Category category) {
			_activeCategories[category] = !_activeCategories[category];
			UpdateFilter();
		}

		private void UpdateFilter() {
			_books = DatabaseContext.GetBooksQueryable().Where(ShouldNotFilterAway).OrderBy(book => book.Author)
				.ThenBy(book => book.Series).ThenBy(book => book.Nr).ToList();

			if (_books.Count < _grid.PageSize * (_grid.CurrentPage + 1))
				_grid.FirstPage();
		}

		private bool ShouldNotFilterAway(Book book) {
			if (_selectedFilterMode == LogicalFilterOperator.And) {
				if (_selectedRating != 0 && _selectedRating != book.Rating)
					return false;
				if (_showDelivered && book.Status != Status.Delivered)
					return false;
				if (_showOrdered && book.Status != Status.Ordered) return false;

				if (_showNone && book.Status != Status.None) return false;
				if (_activeCategories[Category.Gore] && !book.Categories.HasFlag(Category.Gore)) return false;
				if (_activeCategories[Category.Gag] && !book.Categories.HasFlag(Category.Gag)) return false;
				if (_activeCategories[Category.Spicy] && !book.Categories.HasFlag(Category.Spicy)) return false;
				if (_activeCategories[Category.Smut] && !book.Categories.HasFlag(Category.Smut)) return false;
				if (_activeCategories[Category.Romance] && !book.Categories.HasFlag(Category.Romance)) return false;
				if (_activeCategories[Category.Fantasy] && !book.Categories.HasFlag(Category.Fantasy)) return false;
				if (_showRead && !book.Read) return false;
				return true;
			}

			if (_selectedRating != 0 && _selectedRating != book.Rating) return false;
			if (_showDelivered && book.Status == Status.Delivered) return true;
			if (_showOrdered && book.Status == Status.Ordered) return true;
			if (_showNone && book.Status == Status.None) return true;
			if (_activeCategories[Category.Gore] && book.Categories.HasFlag(Category.Gore)) return true;
			if (_activeCategories[Category.Gag] && book.Categories.HasFlag(Category.Gag)) return true;
			if (_activeCategories[Category.Spicy] && book.Categories.HasFlag(Category.Spicy)) return true;
			if (_activeCategories[Category.Smut] && book.Categories.HasFlag(Category.Smut)) return true;
			if (_activeCategories[Category.Romance] && book.Categories.HasFlag(Category.Romance)) return true;
			if (_activeCategories[Category.Fantasy] && book.Categories.HasFlag(Category.Fantasy)) return true;
			if (_showRead && book.Read) return true;
			return false;
		}

		public static string GenerateColor(bool value) {
			return !value ? "filter: saturate(0%);" + "width: 25px; height: 25px;" : "width: 25px; height: 25px;";
		}

		public static string GenerateName(Book book) {
			string special = book.Extra == ExtraInfo.Special ? "(SE)" : "";

			return $"{book.Name} {special}";
		}

		private void UpdateBook(Status data, Book book) {
			book.Status = data;
			UpdateBookData(book);
		}


		private void UpdateBookData(Book book) {
			DatabaseContext.Books.Update(book);
			DatabaseContext.SaveChanges();
			if (!ShouldNotFilterAway(book)) {
				_books.Remove(book);
				_grid.Reload();
			}

			StateHasChanged();
		}

		private bool UpdateBook(Book book) {
			if (!_addBook.ValidateBook(book)) return false;

			Series series;
			if (book.Series == null || string.IsNullOrEmpty(book.Series.Name)) {
				series = null;
			}
			else {
				book.Series.Name = AddBook.CleanUpString(book.Series.Name);
				series = DatabaseContext.Series.FirstOrDefault(ser => ser.Name == book.Series.Name) ??
				         new Series { Name = book.Series.Name };
			}

			Book dbBook = DatabaseContext.Books.Include(book1 => book1.Series).First(book1 => book1.Id == book.Id);

			dbBook.Copy(book);
			dbBook.Series = series;

			DatabaseContext.Books.Update(dbBook);
			DatabaseContext.SaveChanges();
			RemoveOldSeries();
			_grid.Reload();
			NotificationService.Notify(new NotificationMessage {
				Duration = NotificationTime, Severity = NotificationSeverity.Success, Summary = "Success:",
				Detail = $"Updated {book.Name}."
			});
			UpdateFilter();
			return true;
		}

		protected override void OnInitialized() {
			_books = DatabaseContext.GetBooks();
			foreach (Category category in _categories) {
				_activeCategories.Add(category, true);
			}
		}

		private void RemoveOldSeries() {
			List<Series> toBeRemoved = DatabaseContext.Series.Include(series => series.Books)
				.Where(series => series.Books.Count == 0).ToList();

			DatabaseContext.Series.RemoveRange(toBeRemoved);
			DatabaseContext.SaveChanges();
			StateHasChanged();
		}

		private void Delete(Book book) {
			DatabaseContext.Remove(book);
			DatabaseContext.SaveChanges();
			RemoveOldSeries();
			_books.Remove(book);
			_grid.Reload();
			NotificationService.Notify(new NotificationMessage {
				Duration = NotificationTime, Severity = NotificationSeverity.Success, Summary = "Success:",
				Detail = $"Deleted {book.Name}."
			});
		}

		private async Task DownloadFile() {
			if (await JSRuntime.InvokeAsync<bool>("confirm", $"Do you want to Export?")) {
				var stream = new MemoryStream();
				await using (var writeFile = new StreamWriter(stream, leaveOpen: true)) {
					var csv = new CsvWriter(writeFile, CultureInfo.InvariantCulture);
					await csv.WriteRecordsAsync(DatabaseContext.Books.ToList());
				}

				stream.Position = 0;
				var fileName = $"Books{DateTime.Now.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture)}.csv";
				await JSRuntime.InvokeAsync<object>("saveAsFile", fileName, Convert.ToBase64String(stream.ToArray()));
			}
		}

		public static string GetCategoryImagePath(Category category) {
			return $"assets/categories/{category.ToString().ToLower()}.png";
		}
	}
}