using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;

namespace XenView.Data {
	public class DatabaseContext : DbContext {
		public DbSet<Series> Series { get; set; }
		public DbSet<Book> Books { get; set; }

		public List<Book> GetBooks() {
			IIncludableQueryable<Book, Series> books = Books.Include(book => book.Series);

			return books.OrderBy(book => book.Author.ToLower()).ThenBy(book => book.Series).ThenBy(book => book.Nr)
				.ToList();
		}

		public List<AuthorTemp> GetAuthors() {
			var authors = new List<AuthorTemp>();
			foreach (string author in Books.Select(book => book.Author).Distinct()) {
				if (authors.Any(temp => temp.Author.Trim() == author.Trim())) continue;
				authors.Add(new AuthorTemp(author.Trim()));
			}

			return authors;
		}

		public DatabaseContext(DbContextOptions<DatabaseContext> contextOptions) : base(contextOptions) { }

		public int GetCount(Category category) {
			return Books.Count(book => book.Categories.HasFlag(category));
		}

		public IIncludableQueryable<Book, Series> GetBooksQueryable() {
			return Books.Include(book => book.Series);
		}
	}
	


	public record AuthorTemp {
		public string Author;

		public AuthorTemp(string author) {
			Author = author;
		}

		public override string ToString() {
			return Author;
		}
	}
}