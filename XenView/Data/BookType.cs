﻿using System;

namespace XenView.Data {
	public enum BookType {
		Unknown,
		PaperBack,
		HardCover,
		Online
	}

	public static class Utility {

		public static BookType FromString(string name) {
			return name.ToLower().Replace(" ", "") switch {
				"unknown" => BookType.Unknown,
				"paperback" => BookType.PaperBack,
				"hardcover" => BookType.HardCover,
				"online" => BookType.Online,
				_ => throw new ArgumentOutOfRangeException(name)
			};
		}
	}
}