﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace XenView.Data {
	public class Series : IComparable<Series> {
		[Key] public string Name { get; set; }

		public List<Book> Books { get; set; }

		public int CompareTo(Series other) {
			if (ReferenceEquals(this, other)) return 0;
			return ReferenceEquals(null, other) ? 1 : string.Compare(Name, other.Name, StringComparison.InvariantCultureIgnoreCase);
		}
	}
}