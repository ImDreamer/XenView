﻿namespace XenView.Data {
	public enum Status {
		None,
		Ordered,
		Delivered
	}
}