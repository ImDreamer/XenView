﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace XenView.Data {
	public class Book {
		[Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int Id { get; set; }

		public string Name { get; set; } = "";
		public Series Series { get; set; }
		public float Nr { get; set; }
		public string Author { get; set; } = "";
		public string ImageLink { get; set; }
		public ExtraInfo Extra { get; set; }

		public Status Status { get; set; }

		public bool Read { get; set; }

		public Category Categories { get; set; }

		public int Rating { get; set; }

		public BookType BookType { get; set; }

		public Book() { }

		public Book(Book book) {
			Copy(book);
		}

		public void Copy(Book book) {
			Id = book.Id;
			Name = book.Name;
			if (book.Series?.Name != null)
				Series = new Series { Name = book.Series.Name };
			Nr = book.Nr;
			Author = book.Author;
			ImageLink = book.ImageLink;
			Extra = book.Extra;
			Status = book.Status;

			Categories = book.Categories;

			Read = book.Read;
			Rating = book.Rating;
			BookType = book.BookType;
		}
	}

	[Flags]
	public enum Category : short {
		None = 0,
		Gore = 1,
		Gag = 2,
		Smut = 4,
		Spicy = 8,
		Romance = 16,
		Fantasy = 32
	}
}