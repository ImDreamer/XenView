﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace XenView.Migrations
{
    public partial class Category : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Categories",
                table: "Books",
                type: "INTEGER",
                nullable: false,
                defaultValue: 0);
                        
            migrationBuilder.Sql("update Books SET Categories = Categories + 1 where Gore = 1;");
            migrationBuilder.Sql("update Books SET Categories = Categories + 2 where Gag = 1;");
            migrationBuilder.Sql("update Books SET Categories = Categories + 4 where EighteenPlus = 1;");
            migrationBuilder.Sql("update Books SET Categories = Categories + 8 where Spicy = 1;");
            
            migrationBuilder.DropColumn(
                name: "EighteenPlus",
                table: "Books");

            migrationBuilder.DropColumn(
                name: "Gag",
                table: "Books");

            migrationBuilder.DropColumn(
                name: "Gore",
                table: "Books");

            migrationBuilder.DropColumn(
                name: "Spicy",
                table: "Books");
            
            
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Categories",
                table: "Books",
                newName: "Spicy");

            migrationBuilder.AddColumn<bool>(
                name: "EighteenPlus",
                table: "Books",
                type: "INTEGER",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Gag",
                table: "Books",
                type: "INTEGER",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Gore",
                table: "Books",
                type: "INTEGER",
                nullable: false,
                defaultValue: false);
        }
    }
}
