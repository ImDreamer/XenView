﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace XenView.Migrations
{
    public partial class MoreData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "EighteenPlus",
                table: "Books",
                type: "INTEGER",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Gag",
                table: "Books",
                type: "INTEGER",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Gore",
                table: "Books",
                type: "INTEGER",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Spicy",
                table: "Books",
                type: "INTEGER",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "EighteenPlus",
                table: "Books");

            migrationBuilder.DropColumn(
                name: "Gag",
                table: "Books");

            migrationBuilder.DropColumn(
                name: "Gore",
                table: "Books");

            migrationBuilder.DropColumn(
                name: "Spicy",
                table: "Books");
        }
    }
}
