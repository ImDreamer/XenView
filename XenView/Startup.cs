using System;
using System.IO;
using System.Threading.Tasks;
using ElectronNET.API;
using ElectronNET.API.Entities;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Radzen;
using XenView.Data;

namespace XenView {
	public class Startup {
		public Startup(IConfiguration configuration) {
			Configuration = configuration;
		}

		public IConfiguration Configuration { get; }

		// This method gets called by the runtime. Use this method to add services to the container.
		// For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
		public void ConfigureServices(IServiceCollection services) {
			services.AddRazorPages();
			services.AddServerSideBlazor();
			services.AddScoped<DialogService>();
			services.AddScoped<NotificationService>();
			services.AddScoped<TooltipService>();
			services.AddScoped<ContextMenuService>();
			DirectoryInfo path = Directory.CreateDirectory(
				Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) +
				"/XenView/Data");
			services.AddDbContext<DatabaseContext>(options => {
				options.EnableSensitiveDataLogging();
				options.UseSqlite($"Data Source = {path}/data.db");
			});
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IWebHostEnvironment env, DatabaseContext db) {
			if (env.IsDevelopment()) {
				app.UseDeveloperExceptionPage();
			}
			else {
				app.UseExceptionHandler("/Error");
				// The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
				app.UseHsts();
			}

			db.Database.EnsureCreated();
			db.Database.Migrate();
			app.UseHttpsRedirection();
			app.UseStaticFiles();

			app.UseRouting();

			app.UseEndpoints(endpoints => {
				endpoints.MapBlazorHub();
				endpoints.MapFallbackToPage("/_Host");
			});
			if (!HybridSupport.IsElectronActive) return;
			SetupMenus();
			Task.Run(async () => await Electron.WindowManager.CreateWindowAsync());
		}

		private static void SetupMenus() {
			var menu = new MenuItem[] {
				new() {
					Label = "Edit", Submenu = new MenuItem[] {
						new() { Label = "Undo", Accelerator = "CmdOrCtrl+Z", Role = MenuRole.undo },
						new() { Label = "Redo", Accelerator = "Shift+CmdOrCtrl+Z", Role = MenuRole.redo },
						new() { Type = MenuType.separator },
						new() { Label = "Cut", Accelerator = "CmdOrCtrl+X", Role = MenuRole.cut },
						new() { Label = "Copy", Accelerator = "CmdOrCtrl+C", Role = MenuRole.copy },
						new() { Label = "Paste", Accelerator = "CmdOrCtrl+V", Role = MenuRole.paste },
						new() { Label = "Select All", Accelerator = "CmdOrCtrl+A", Role = MenuRole.selectall }
					}
				},
				new() {
					Label = "View", Submenu = new MenuItem[] {
						new() {
							Role = MenuRole.reload
						},
						new() {
							Role = MenuRole.forcereload
						},
						new() {
							Role = MenuRole.toggledevtools
						},
						new() {
							Type = MenuType.separator
						},
						new() {
							Type = MenuType.separator
						},
						new() {
							Role = MenuRole.resetzoom
						},
						new() {
							Role = MenuRole.zoomin
						},
						new() {
							Role = MenuRole.zoomout
						},
						new() {
							Type = MenuType.separator
						},
						new() {
							Role = MenuRole.togglefullscreen
						}
					}
				},
				new() {
					Label = "Window", Role = MenuRole.window, Submenu = new MenuItem[] {
						new() { Label = "Minimize", Accelerator = "CmdOrCtrl+M", Role = MenuRole.minimize },
						new() { Label = "Close", Accelerator = "CmdOrCtrl+W", Role = MenuRole.close }
					}
				}
			};
			Electron.Menu.SetApplicationMenu(menu);
		}
	}
}